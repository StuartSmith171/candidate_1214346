**********************************************
*      Date Created: 24th June 2020          *
*      Author: Stuart Smith                  *
*      Candidate Number: 1214346             *
*                                            *
*      Home Office Technical Assessment      *
**********************************************

**********************************************
*            What's Included                 *
**********************************************

1. Folder "candidate-1214346-home-office-technical-assessment.zip" - This contains the Maven project in ZIP format, will need to be unzipped to run.
2. Zip Folder - "candidate-1214346-target" - This contains the results of the project being run (Chrome Only).
3. File - "README.txt" - Overview File.

**********************************************
*                 How to Run                 *
**********************************************

1. Download 'candidate-1214346-home-office-technical-assessment.zip'
2. Open a Command Prompt.
3. Navigate to the directory of the project: `candidate-1214346-home-office-technical-assessment'.
4. Run with command: 'mvn clean verify'.
5. Report file 'index.html' should be generated in folder "candidate-1214346-home-office-technical-assessment/target/site/serenity".

**********************************************
*                   Tests                    *
**********************************************

The following are the list of tests which are run as part of project.

Test 1. Verify if a Japanese national studying for over 6 months requires a visa
Test 2. Verify if a Japanese national visiting as a tourist requires a visa
Test 3. Verify if a Russian national visiting as a tourist with no partner or family requires a visa.
Test 4. Verify if sending an API request produces a "Status": 200 response.

**********************************************
*                   Notes                    *
**********************************************

1. Project is configured to run on Chrome only, to change, open file "serenity.properties" in main directory and delete line 3 ('webdriver.driver=chrome')
2. "Test 2" has a result for the test to pass. Some confusion with the original e-mail which stated "Then I will be informed “I won’t need a visa to study in the UK”" - this would cause the test to fail as the steps are for a visit of Tourism. Should this be the actual response, then Line 98 of file 'ConfirmVisaRequiredDefinitions.java' could have "come" changed to "study". The test will then fail.

*********************************************
*               Improvements                *
*********************************************

The following improvements could be made to the scripts:

Script: 'ConfirmVisaRequiredDefinitions.java':
* Clicking the 'Next Step' button could be made into it's own "And" step, however this would likely cause the business scenarios to be overly long. To get round this, it was put at the start of each step after the user chooses a nationality. This ensures the "When" step is a definitive progress to final page as the responses ar enot linear.
* Variables could be passed for: Nationality (i.e. pass 'Japan' and 'Russia' as variables into the one class instead of having classes for each instance.
* Variables could be used more extensively for responses on the different pages.
* May be easier to maintain if the steps were ina separate file whcih was referenced in this file so this file just had the high-level responses.
* Use asserts for each Given/When/Then step to provide 'Failures' instead of 'Errors' if something goes wrong.
* Have an initial step where the website is opened, instead of doing it in the 'define nationality' steps.

Script 'ConfirmPostCodeResponse.java':
* Should have a more elegant method instead of re-calling the same line of code twice (but with an added section). Or perhaps not suited for Given/Then
* Could reference the API so that the actual postcode is passed as a variable to test multiple postcodes (this is however out of scope)


*********************************************
*                 Directory                 *
*********************************************

- candidate-1214346-home-office-technical-assessment
--| src
----| test
------| java
--------| homeOffice
----------| postcode
------------ ConfirmPostCodeResponse.java		- Steps to execute Test 1, written in Java using REST API
----------| visa
------------ ConfirmVisaRequiredDefinitions.java	- Step definitions to execute Test 1,2,3. Written in Java using Selenium Webdriver
---------- CucumberTestSuite.java			- Test Runner for verifying the Cucumber steps
------| resources
--------| features
----------| postcode
------------ confirm_postcode_response.feature		- Feature description and Cucumber steps for verifying test 4
----------| visa
----------- confirm_if_visa_required.feature		- Feature description and Cucumber steps for verifying tests 1 - 3
